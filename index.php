<!DOCTYPE HTML> 
<html lang=en>
<head></head>
<style>
body{
font:normal 14px "Helvetica Neue",Helvetica,Arial,sans-serif;
padding:50px;
color:#444
}
ul{
list-style:none;
}
div{
width:250px;
margin:0 auto
}
h1{
text-align:center;
}
strong{
display:inline-block;
text-align:right;
width:135px
}
hr{
width:70px;
margin:30px auto;
border-width:1px 0 0;
border-color:#34495E
}
</style>
<body>
	<div>
		<h1>Poll</h1>
		<hr />
		<p>Which is your favorite football club?</p>
		<ul>
			<li>1. Manchester United</li>
			<li>2. Chelsea</li>
			<li>3. Arsenal</li>
			<li>4. Liverpool.</li>
		</ul>
		<!--p>Dial +18325351829 to vote.</p-->
		<p>Dial (+234) (0) 14405102 to vote.</p>
		<h1>Result</h1>
		<hr />
		<ul style="margin:0;padding:0">
			<?php
			mysql_connect("localhost", "root", "____");
			mysql_select_db("polls");

			$clubs = array(
				'1' => 'Manchester United',
				'2' => 'Chelsea',
				'3' => 'Arsenal',
				'4' => 'Liverpool'
			);

			$r = mysql_query("select * from votes");
			while(list($id, $hit) = mysql_fetch_array($r)) {
				?>
				<li><p><strong><?= $clubs[$id]; ?></strong>: <?= $hit; ?> votes</p></li>
				<?php
			}
			?>
		</ul>
	</div>
</body>
</html>